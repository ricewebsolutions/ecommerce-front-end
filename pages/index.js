import Head from 'next/head'
import styles from '../styles.css'

export default function Home() {
  return (
    <Head>
      <h1>Ecommerce</h1>
    </Head>
  )
}
